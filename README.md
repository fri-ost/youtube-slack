# `youtube-slack` – fetch a YouTube playlist and post new entries to Slack

A simple script that looks for new entries on a YouTube playlist and posts
these as links to Slack.


# Installation and requirements

Simply clone this repository or download the `ys.py` file. To run it, you need
to have Python 3 installed on your system.

It's up to yourself to find out how and when to run it automatically. An
obvious way is to set up a cronjob to run the script at a desired interval.

Before running the script for the first time, you need to create a
configuration file.

## Configuration

You must create a `config.json` file next to the `ys.py` script, in order for
it to work. Here's a full example configuration:

```
{
    "last-posted-filename": "last-posted.dat",
    "youtube-playlist-id": "<PLAYLIST_ID>",
    "slack-webhook": "https://hooks.slack.com/services/<XXX>/<YYY>/<ZZZ>",
    "slack-message-templates": "A new track has been added to the playlist: <{url}|{title}>",
    "slack-channel": "#music"
}
```

### `last-posted-filename`

Optional. Default value shown in above example.

To remember what has already been posted, the ID of the most recently posted
video is saved to this file, next to the `ys.py` script.

### `youtube-playlist-id`

Mandatory.

Find the ID of the playlist in your browser, and put it here.

### `slack-webhook`

Mandatory.

Put the Slack Incoming Webhook URL here.

### `slack-message-templates`

Optional. Default value shown in above example.

What the message posted to Slack will look like. Can be a string or a list of
strings, of which a random will be chosen. The two `{xyz}` pairs of curly
brackets are replaced by the video URL and title, respectively. The
`<{url}|{title}>` pattern will turn that into a nice link in Slack, and Slack
will even automatically load an interactive preview of the video.

### `slack-channel`

Optional. Will post to the default channel of the Slack Incoming Webhook if not
specified.

Where to post the message. Can be a `#channel` or directly to a `@username`.
