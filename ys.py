#!/usr/bin/env python3

"""Fetch a YouTube playlist and post new entries to Slack."""

import datetime
import json
import os
from pathlib import Path
import random
import sys
from urllib import (
    error,
    parse,
    request,
)
from xml.dom import minidom

ROOT = os.path.dirname(__file__)
ERROR_YOUTUBE_NON_200_RESPONSE = 1
ERROR_MISSING_MANDATORY_SETTING = 2
ERROR_LOCK_FILE_EXISTS = 3
ERROR_YOUTUBE_TIMEOUT = 4
ERROR_UNKNOWN_EXCEPTION = 1000
TIMEOUT = 10


def load_config():
    """Load the configuration file and fill in defaults."""
    # Load configuration from file.
    with open(os.path.join(ROOT, "config.json"), "r") as f:
        config = json.loads(f.read())

    # Check that mandatory settings are present.
    mandatories = ["slack-webhook", "slack-channel", "youtube-playlist-id"]
    for m in mandatories:
        if m not in config:
            print(
                "Mandatory setting '{}' not found in configuration. "
                "Aborting.".format(m)
            )
            exit(ERROR_MISSING_MANDATORY_SETTING)

    # Fill in defaults.
    defaults = {
        "last-posted-filename": "last-posted.dat",
        "slack-message-templates": (
            "A new track has been added to the playlist: <{url}|{title}>"
        ),
        "lock-filename": "lock-file.tmp",
    }
    for d in defaults:
        if config.get(d) is None:
            config[d] = defaults.get(d)

    # Make sure message templates is a list.
    if type(config.get("slack-message-templates")) == str:
        config["slack-message-templates"] = [
            config.get("slack-message-templates")
        ]

    return config


def get_playlist_data(playlist_id):
    """Get playlist data from YouTube."""
    try:
        response = request.urlopen(
            "{}{}".format(
                "https://www.youtube.com/feeds/videos.xml?playlist_id=",
                playlist_id,
            ),
            timeout=TIMEOUT,
        )
    except error.URLError:
        print(
            "YouTube did not respond before timeout of {} was reached.".format(
                TIMEOUT
            )
        )
        sys.exit(ERROR_YOUTUBE_TIMEOUT)
    if response.status != 200:
        print(
            "YouTube returned a non-200 response status ({}). "
            "Aborting.".format(response.status)
        )
        sys.exit(ERROR_YOUTUBE_NON_200_RESPONSE)

    # Extract playlist data and iterate over entries.
    data = minidom.parseString(response.read())
    return data.getElementsByTagName("entry")


def get_last_posted_ids(file_name):
    """Read the IDs of the last posted videos from storage file."""

    # Get the ID of the most recently posted video from the storage file, if
    # any. Otherwise create it.
    try:
        with open(os.path.join(ROOT, file_name), "r") as f:
            video_ids = f.read().strip().split(",")
    except FileNotFoundError:
        video_ids = []
        print(
            "Is this the first run? Storage file ({}) doesn't exist. Creating "
            "it now.".format(file_name)
        )
        open(os.path.join(ROOT, file_name), "w").close()

    return video_ids


def lock(file_name):
    """
    Handle lock file initialization.

    To make sure we only have a single instance running at the same time,
    we create a lock file. If this file already exists, we abort the further
    processing.
    """
    lock_file = Path(__file__).resolve().parent.joinpath(file_name)
    if lock_file.is_file():
        print(
            "Lock file exists. Another instance is probably already "
            "running. Aborting."
        )
        exit(ERROR_LOCK_FILE_EXISTS)
    else:
        with open(lock_file, "w") as f:
            f.write(str(datetime.datetime.now()))


def unlock(file_name):
    """Remove the lock file."""
    lock_file = Path(__file__).resolve().parent.joinpath(file_name)
    lock_file.unlink()


# Let's do it!

try:
    print("\n{} (youtube-slack)".format(datetime.datetime.now()))
    config = load_config()
    lock(config.get("lock-filename"))
    entries = get_playlist_data(config.get("youtube-playlist-id"))
    last_posted_ids = get_last_posted_ids(config.get("last-posted-filename"))
    return_code = 0
    last_video_id = None
    new_entries = []

    # Entries come with newest first. Start by separating new entries from
    # all entries.
    for entry in entries:
        # Get the video ID.
        video_id = entry.getElementsByTagName("yt:videoId")[0].firstChild.data

        # If this has already been posted, we don't need to continue.
        if video_id in last_posted_ids:
            break
        new_entries.append(entry)
        last_posted_ids.append(video_id)

    # Nothing to do if we don't have any new entries.
    if not new_entries:
        print("Nothing new here. Our job here is done.")
    else:
        # Reverse the order of new entries, to have the oldest first.
        new_entries.reverse()

        # Then post new entries to Slack.
        for entry in new_entries:
            # Get the video ID, title and URL.
            video_id = entry.getElementsByTagName("yt:videoId")[
                0
            ].firstChild.data
            last_video_id = video_id
            title = entry.getElementsByTagName("title")[0].firstChild.data
            url = entry.getElementsByTagName("link")[0].getAttribute("href")

            # Construct a Slack payload.
            template = random.choice(config.get("slack-message-templates"))
            payload = {
                "payload": {"text": template.format(url=url, title=title)}
            }
            if config.get("slack-channel") is not None:
                payload.get("payload").update(
                    {"channel": config.get("slack-channel")}
                )
            payload = parse.urlencode(payload).encode("utf-8")

            # Call the Slack hook with the payload.
            hook = request.Request(
                config.get("slack-webhook"), data=payload, method="POST"
            )
            response = request.urlopen(hook)
            if response.status == 200:
                print("Posted '{}' ({}) to Slack.".format(title, video_id))
            else:
                print(
                    "Unable to post '{}' ({}) to Slack. Response status code "
                    "was {}.".format(title, video_id, response.status)
                )

            # Store the ID of the last video for the next run.
            with open(
                os.path.join(ROOT, config.get("last-posted-filename")), "w"
            ) as f:
                f.write(",".join(last_posted_ids[-20:]))
except Exception as e:
    print("Oh noes! An exception was raised :(")
    return_code = ERROR_UNKNOWN_EXCEPTION
finally:
    unlock(config.get("lock-filename"))

sys.exit(return_code)
